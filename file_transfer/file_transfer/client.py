# 导入socket
import socket


def main():
    '''客户端主函数
        接收传送过来的文件
    '''
    # 创建套接字
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_ip = input("请输入对方ip:")
    server_port = -1
    try:
        server_port = int(input("请输入对方端口号:"))
    except:
        print("输入有误！")
    try:
        # 连接服务端
        client_socket.connect((server_ip,server_port))
    except:
        print("连接服务器失败")
        client_socket.close()
        return
    file_name = input("请输入要下载的文件名:")
    client_socket.send(file_name.encode())
    file_context = None
    while True:
        recv_data = client_socket.recv(1024)
        if recv_data:
            # 创建文件
            file_context = open("[新]" + file_name, "ab")
            file_context.write(recv_data)
        else:
            if file_context:
                file_context.close()
            break
    # 关闭套接字
    client_socket.close()


if __name__ == "__main__":
    main()