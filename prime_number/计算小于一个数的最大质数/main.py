try:
    n=int(input("请输入一个数"))
    result=0
    # n>2意义
    if n<3:
        print("输入有误!")
    elif n==3:
        print('这个数是2')
    else:
        # 从小于n的最后一个数进行排查
        for i in range(n-1,1,-1):
            # 检验当前的数是否是质数
            for j in range(2,i):
                if i%j==0:  # 如果被0整数则不是质数,继续检验下一个数
                    break
                elif j==i-1: # 如果循环进行到最后说明这个数是质数
                    result=i
            if result !=0:  # 如果result不为0,说明已经找到了这个质数
                break
        print('这个数是%d'%result)
except:
    # 如果输入的不是整数类型
    print("输入有误!")