# 导入socket
import socket


def main():
    '''服务端主函数
        将文件传送到客户端
    '''
    # 创建套接字
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # 绑定ip和端口
    server_socket.bind(("",8088))
    # 将client_socket变为服务端
    server_socket.listen()
    while True:
        # 等待客户端连接
        client_server_socket, add_data = server_socket.accept()
        # 接收客户端发送的文件名
        recv_data = client_server_socket.recv(1024)
        print(recv_data.decode())
        try:
            # 打开文件
            file = open(recv_data.decode(), "rb")
            print("打开文件成功!")
            while True:
                readline = file.read(1024)
                if not readline:
                    file.close()
                    print("关闭文件成功!")
                    break
                client_server_socket.send(readline)
        except:
            print("文件不存在")
        break
    # 关闭套接字
    server_socket.close()


if __name__ == "__main__":
    main()